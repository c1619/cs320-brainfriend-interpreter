#include <iostream>
#include <fstream>
#include <string>

#define NUM_BYTES 30000

using namespace std;

class Interpreter {
    fstream srcCode;   // file with the brainfriend code   
    char bytes [NUM_BYTES] = {0}; // The 30k bytes for memory
    int bytePtr;    // keeping track of index of memory
    int numBrackets; // keeps track of number of brackets that has been come across
    int numBracketsAtJump;
    bool jumpingBack;       // keeps track of whether to jump backwards to search for the corresponding opening bracket 
    bool jumpingForward;    // keeps track of whether to jump forwards to search for the corresponding closing bracket

    // Public contains the constructor and the run function only. These should be the only forms through which the user will interact with the Interpreter class. 
    public:
        Interpreter(string fileName) {
            jumpingBack = false;
            jumpingForward = false;
            bytePtr = 0;
            numBrackets = 0;
            srcCode.open(fileName, ios::in);
            if(srcCode.is_open() != true) {
                cout << "ERROR: Invalid File Name Provided" << endl;
                exit(1);
            }
        }
        void run() {
            char charInp;
            while(srcCode.good()) {
                if(jumpingBack) {
                    // setting the file pointer 2 characters back before reading so we're essentially reading one space backwards from where we were
                    // (note that the get function places the file pointer one spot forward)
                    srcCode.seekg(-2, ios::cur);
                    charInp = srcCode.get();
                } else {
                    charInp = srcCode.get();
                }
            
                if(charInp == '>' && !jumpingBack && !jumpingForward) {
                    inc_dp();
                } else if(charInp == '<' && !jumpingBack && !jumpingForward) {
                    dec_dp();                    
                } else if(charInp == '+' && !jumpingBack && !jumpingForward) {
                    inc_data();
                } else if(charInp == '-' && !jumpingBack && !jumpingForward) {
                    dec_data();
                } else if(charInp == '.' && !jumpingBack && !jumpingForward) {
                    output();
                } else if(charInp == ',' && !jumpingBack && !jumpingForward) {
                    input();
                } else if(charInp == '[') {
                    loop();
                } else if(charInp == ']') {
                    repeat();
                } else {
                    continue;
                }
            }
        }
    
    private:
        void inc_dp() { // corresponds to ‘>’
            ++bytePtr;
        }       

        void dec_dp() { // corresponds to ‘<‘
            --bytePtr;
        }  

        void inc_data() {  // corresponds to ‘+’
            ++bytes[bytePtr];
        } 

        void dec_data() {  // corresponds to ‘-’
            --bytes[bytePtr];
        }  

        void output() const { // corresponds to ‘.’
            cout << bytes[bytePtr];
        }

        void input() {  // corresponds to ‘,’
            char input;
            input = getchar();
            bytes[bytePtr] = input; 
        }

        void loop() {   // corresponds to ‘[‘
            ++numBrackets;
            
            // if we are jumping back to find the corresponding opening bracket '[' and this is in fact the correct bracket (we know if it is depending on the bracketCount), then we stop jumping back
            if(jumpingBack && numBrackets >= 1) {
                jumpingBack = false;
            }

            // If we are jumping back to find the corresponding opening bracket '[' and this is not the correct bracket (i.e numBrackets <= 0) then we just ignore and go back further
            if(jumpingBack && numBrackets <= 0) {
                return;
            }
            
            // If we are jumpingForward, we can simply ignore this bracket after we have incremented numBrackets
            if(jumpingForward) {
                return;
            }

            if(bytes[bytePtr] != 0) {   // If current byte is not 0, continue running instructions
                return;
            }

            // If current byte is 0, jump forward to the corresponding closing bracket ']'
            jumpingForward = true;
            numBracketsAtJump = numBrackets;
            return;
        }  

        void repeat() {     // corresponds to ']'
            --numBrackets;

            if(jumpingForward && numBrackets == (numBracketsAtJump - 1)) {    // if we are jumping forward to find a corresponding closing bracket and we have found it, continue running instructions
                jumpingForward = false; // We are no longer jumping forward but rather simply running instructions.
                return;
            }
            
            // If we are jumping forwards to the corresponding closing bracket and this is not the corresponding closing bracket then we just ignore and move on.
            // If we are jumping back then we just continue backwards after we decremented numBrackets until we find the corresponding opening bracket [
            if(jumpingForward || jumpingBack) {
                return;
            }

            // If we are not jumping forward and the current byte is not 0 then we jump back to the corresponding open bracket [
            if(bytes[bytePtr] != 0) {
                jumpingBack = true;
            }

            return;
        }          
};

int main(int argc, char **argv) {
    // Making sure the user entered specifically only one bf file 
    if(argc == 1 || argc >= 3) {
        cout << "Usage: ./brainfriend <bf file>" << endl;
        exit(0);
    }
    
    Interpreter bfInterpreter(argv[1]);
    bfInterpreter.run();
}